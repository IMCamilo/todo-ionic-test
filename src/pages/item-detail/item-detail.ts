import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html',
})
export class ItemDetailPage {

  title;
  description;
  price;

  constructor(public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItemDetailPage');
    this.title = this.navParams.get('item').title
    this.description = this.navParams.get('item').description
    this.price = this.navParams.get('item').price
  }

}
